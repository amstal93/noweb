# Introduction

This project creates a docker image with noweb installed.

The image can be used as a base image for other images or to create noweb documents.

This repository is mirrored to https://gitlab.com/sw4j-net/noweb
